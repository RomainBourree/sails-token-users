/**
 * UsersController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
  login: function (req, res) {
    var email = req.param('email');
    var password = req.param('password');

    Users.findOne({email: email}, function (err, user) {
      if (!user) {
        return res.json(401, {err: '#L1001'});
      }

      Users.comparePassword(password, user, function (err, valid) {
        if (err) {
          return res.json(403, {err: '#L1002'});
        }

        if (!valid) {
          return res.json(401, {err: '#L1001'});
        } else {
          res.json({
            user: user,
            token: jwToken.issue({id : user.id })
          });
        }
      });
    })
  },

  create: function (req, res) {
    Users.create(req.body).exec(function (err, user) {
      if (err)
        return res.json(400, {err: '#SI1001', detail: err});
      else
        res.json(200, {user: user, token: jwToken.issue({id: user.id})});

    });
  },

  forgot: function (req, res) {

  },

  update: function (req, res) {
    console.log(req.params.id);
    Users.update({idUser: req.params.id},req.body).exec(function (err, update){
      if (err)
        return res.json(err.status, {err: '#UP1001', detail: err});
      else
        return res.json(200, {res: '#UP1000'});
    });
  },
};
