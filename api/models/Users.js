/**
* Users.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

// We don't want to store password with out encryption
var bcrypt = require('bcrypt');

module.exports = {

  schema: true,

  attributes: {
    id: {
      autoIncrement:true,
      primaryKey: true,
      unique: true,
      columnName: 'idUser'
    },
    email: {
      type: 'email',
      required: true,
      unique: true
    },
    password: {
      required: true,
      type: 'string'
    },
    name: {
      type: 'string',
      required: true
    },
    lastname: {
      type: 'string',
      required: true
    },
    picture: {
      type: 'string',
      required: false
    },
    pseudo: {
      type: 'string',
      required: false
    },
    // We don't wan't to send back encrypted password either
       toJSON: function () {
         var obj = this.toObject();
         delete obj.password;
         return obj;
       }
     },
     // Here we encrypt password before creating a User
     beforeCreate : function (values, next) {
       bcrypt.genSalt(10, function (err, salt) {
         if(err) return next(err);
         bcrypt.hash(values.password, salt, function (err, hash) {
           if(err) return next(err);
           values.password = hash;
           next();
         })
       })
     },

     comparePassword : function (password, user, cb) {
       bcrypt.compare(password, user.password, function (err, match) {

         if(err) cb(err);
         if(match) {
           cb(null, true);
         } else {
           cb(err);
         }
       })
     }
   };
