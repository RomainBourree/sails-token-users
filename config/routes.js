
  /*********************/
  /****** Routing ******/
  /*********************/

module.exports.routes = {

  /************************* USERS ****************************/
  'POST     /auth'              : 'UsersController.login',
  'POST     /user'              : 'UsersController.create',
  'POST     /forgot'            : 'UsersController.forgot',
  'PUT      /user/:id'          : 'UsersController.update',
  /************************************************************/

};
